package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

func main() {
	var wg sync.WaitGroup

	var timesWaitingToPay []int64
	maxGoroutines := 2
	guard := make(chan struct{}, maxGoroutines)
	wg.Add(1)
	go stationQueue(4, 50, "gas", 1000, 5000, guard, &timesWaitingToPay, &wg)
	wg.Add(1)
	go stationQueue(4, 50, "diesel", 1000, 5000, guard, &timesWaitingToPay, &wg)
	wg.Add(1)
	go stationQueue(1, 50, "lpg", 1000, 5000, guard, &timesWaitingToPay, &wg)
	wg.Add(1)
	go stationQueue(8, 50, "electric", 3000, 8000, guard, &timesWaitingToPay, &wg)
	wg.Wait()
	//fmt.Println(timesWaitingToPay)
	var count, sum int64
	for _, val := range timesWaitingToPay {
		if val > 0 {
			count += 1
			sum += val
		}
	}
	avg := sum / count
	fmt.Println("Average time waiting to pay", avg, "milliseconds.")
}

func stationQueue(maxCarsAtStation, nCarsInQueue int, stationType string, minMilSecsWait, maxMilSecsWait int, cashierChan chan struct{}, timesWaitingToPay *[]int64, wg *sync.WaitGroup) {
	defer wg.Done()

	var wg2 sync.WaitGroup

	minMilSecsCashRegister, maxMilSecsCashRegister := 500, 2000

	maxGoroutines := maxCarsAtStation
	guard := make(chan struct{}, maxGoroutines)

	for i := 0; i < nCarsInQueue; i++ {
		start := time.Now()
		guard <- struct{}{} // would block if guard channel is already filled
		wg2.Add(1)
		go func(n int, cashierChan chan struct{}, wg *sync.WaitGroup) {
			defer wg.Done()
			car(i, minMilSecsWait, maxMilSecsWait, stationType)
			cashierChan <- struct{}{}
			func() {
				car(i, minMilSecsCashRegister, maxMilSecsCashRegister, "cash register")
				<-cashierChan
			}()
			<-guard
		}(i, cashierChan, &wg2)
		*timesWaitingToPay = append(*timesWaitingToPay, time.Since(start).Milliseconds())
	}
	wg2.Wait()
}

func car(number int, minMilSecs, maxMilSecs int, stationType string) {
	n := rand.Intn(maxMilSecs-minMilSecs) + minMilSecs
	fmt.Println("Car at", stationType, "for", n, "millisecs")
	time.Sleep(time.Duration(n) * time.Millisecond)
}
