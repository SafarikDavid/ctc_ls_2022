package main

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/joho/godotenv"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"net/http"
	"os"
	"time"
)

var client *mongo.Client
var collection *mongo.Collection

type Product struct {
	Id     string  `json:"_id,omitempty" bson:"_id,omitempty"`
	Name   string  `json:"Name,omitempty" bson:"Name,omitempty"`
	Price  float32 `json:"Price,omitempty" bson:"Price,omitempty"`
	Amount int     `json:"Amount,omitempty" bson:"Amount,omitempty"`
}

var Products []Product

func returnAllProducts(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("content-type", "application/json")
	var products []Product
	ctx, _ := context.WithTimeout(context.Background(), 30*time.Second)
	cursor, err := collection.Find(ctx, bson.M{})
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		var product Product
		cursor.Decode(&product)
		products = append(products, product)
	}
	if err := cursor.Err(); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(w).Encode(products)
}

func returnSingleProduct(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("content-type", "application/json")
	params := mux.Vars(r)
	id, _ := params["id"]
	var product Product
	ctx, _ := context.WithTimeout(context.Background(), 30*time.Second)
	err := collection.FindOne(ctx, Product{Id: id}).Decode(&product)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(`{ "message": "` + err.Error() + `" }`))
		return
	}
	json.NewEncoder(w).Encode(product)
}

func createNewProduct(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("content-type", "application/json")
	var products Product
	_ = json.NewDecoder(r.Body).Decode(&products)
	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	result, _ := collection.InsertOne(ctx, products)
	json.NewEncoder(w).Encode(result)
}

func deleteProduct(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("content-type", "application/json")
	params := mux.Vars(r)
	id, _ := params["id"]
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	result, err := collection.DeleteOne(ctx, Product{Id: id})
	if err != nil {
		panic(err)
	}
	fmt.Printf("DeleteOne removed %v document(s)\n", result.DeletedCount)
}

func updateProduct(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("content-type", "application/json")
	params := mux.Vars(r)
	id, _ := params["id"]
	var product Product
	_ = json.NewDecoder(r.Body).Decode(&product)
	ctx, _ := context.WithTimeout(context.Background(), 30*time.Second)
	result, err := collection.ReplaceOne(
		ctx,
		bson.M{"_id": id},
		product,
	)
	if err != nil {
		panic(err)
	}
	fmt.Printf("Updated %v Documents!\n", result.ModifiedCount)
}

func EnvMongoURI() string {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	return os.Getenv("MONGOURI")
}

func handleRequests() {
	fmt.Println("Starting the application...")
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(os.Getenv("MONGOURI")))
	defer func() {
		if err = client.Disconnect(ctx); err != nil {
			panic(err)
		}
	}()
	collection = client.Database("products_database").Collection("products")
	myRouter := mux.NewRouter().StrictSlash(true)

	myRouter.HandleFunc("/products", returnAllProducts).Methods("GET")
	myRouter.HandleFunc("/search/{id}", returnSingleProduct).Methods("GET")
	myRouter.HandleFunc("/product", createNewProduct).Methods("POST")
	myRouter.HandleFunc("/update/{id}", updateProduct).Methods("POST")
	myRouter.HandleFunc("/delete/{id}", deleteProduct).Methods("DELETE")

	panic(http.ListenAndServe(":8080", myRouter))
}

func main() {
	handleRequests()
}
