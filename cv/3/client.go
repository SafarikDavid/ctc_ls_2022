package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

func main() {
	get()
	post()
	get()
	get_one("1")
	update()
	get()
	delete("1")
	get()
}

type Product struct {
	Id     string  `json:"_id,omitempty" bson:"_id,omitempty"`
	Name   string  `json:"Name,omitempty" bson:"Name,omitempty"`
	Price  float32 `json:"Price,omitempty" bson:"Price,omitempty"`
	Amount int     `json:"Amount,omitempty" bson:"Amount,omitempty"`
}

func get() {
	fmt.Println("Get...")
	resp, err := http.Get("http://localhost:8080/products")
	if err != nil {
		panic(err)
	}

	// !!!
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		panic(err)
	}

	fmt.Println(string(body))
}

func get_one(id string) {
	fmt.Println("Get one...")
	resp, err := http.Get("http://localhost:8080/search/" + id)
	if err != nil {
		panic(err)
	}

	// !!!
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		panic(err)
	}

	fmt.Println(string(body))
}

func post() {
	fmt.Println("Posting...")
	json_data, err := json.Marshal(Product{Id: "1", Name: "Milk", Price: 23.50, Amount: 1})

	if err != nil {
		panic(err)
	}
	resp, err := http.Post("http://localhost:8080/product", "application/json",
		bytes.NewBuffer(json_data))
	if err != nil {
		panic(err)
	}

	// !!!
	defer resp.Body.Close()
}

func delete(id string) {
	fmt.Println("Deleting...")
	req, err := http.NewRequest(http.MethodDelete, "http://localhost:8080/delete/"+id, nil)
	if err != nil {
		panic(err)
	}

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}

	defer resp.Body.Close()
}

func update() {
	fmt.Println("Updating...")
	json_data, err := json.Marshal(Product{Id: "1", Name: "Ketchup", Price: 37.50, Amount: 1})

	if err != nil {
		panic(err)
	}
	resp, err := http.Post("http://localhost:8080/update/1", "application/json",
		bytes.NewBuffer(json_data))
	//resp, err := http.NewRequest(http.MethodPut, "http://localhost:8080/update/1", bytes.NewBuffer(json_data))
	if err != nil {
		panic(err)
	}

	// !!!
	defer resp.Body.Close()
}
