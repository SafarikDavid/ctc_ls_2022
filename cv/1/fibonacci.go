package main

import "fmt"

// fibonacci is a function that returns
// a function that returns an int.
func fibonacci() func() int {
	n := 0
	previous := 0
	actual := 1
	temp := 0
	return func() int {
		if n == 0 {
			n += 1
			return 0
		}
		if n == 1 {
			n += 1
			return 1
		}
		n += 1
		temp = previous + actual
		previous = actual
		actual = temp
		return actual
	}
}

func main() {
	f := fibonacci()
	for i := 0; i < 10; i++ {
		fmt.Println(f())
	}
}
