package main

import (
	"github.com/spf13/cobra"
	"gitlab.com/SafarikDavid/ctc_ls_2022/cv/5/cmd/client"
	"gitlab.com/SafarikDavid/ctc_ls_2022/cv/5/cmd/server"
	"gitlab.com/SafarikDavid/ctc_ls_2022/cv/5/pkg/util"
)

func main() {
	cmd := &cobra.Command{
		Use: "ctc-05",
		CompletionOptions: cobra.CompletionOptions{
			DisableDefaultCmd: true,
		},
	}

	cmd.AddCommand(server.Cmd(), client.Cmd())

	util.ExitOnError(cmd.Execute())
}
